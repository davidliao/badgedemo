import sys
import os
import time
from PIL import Image
from PIL import ImageOps
from PIL import ImageDraw
from PIL import ImageFont
from EPD import EPD



# fonts are in different places on Raspbian/Angstrom so search
possible_fonts = [
    '/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf',               #Desbian B.B
    '/usr/share/fonts/truetype/liberation/LiberationMono-Bold.ttf',      #Desbian B.B
    '/usr/share/fonts/truetype/tff-dejavu/DejaVuSansMono-Bold.ttf',      #R.Pi
    'usr/share/fonts/truetype/freefont/FreeMono.ttf',                    #R.Pi
    'usr/share/fonts/truetype/LiberationMono-Bold.ttf',                  #B.B
    'usr/share/fonts/truetype/DejaVuSansMono-Bold.ttf'
]

FONT_FILE=''
for f in possible_fonts:
  if os.path.exists(f):
        FONT_FILE = f
        break

if '' == FONT_FILE:
    raise 'no font file found'

FONT_SIZE = 25

VISITOR_NAME = 'DUCK'
VISITOR_OCCUPATION = 'PET'

def main(argv):
    """main program - display list of images"""

    epd = EPD()

    epd.clear()

    print('panel = {p:s} {w:d} x {h:d}  version={v:s}'.format(p=epd.panel, w=epd.width, h=epd.height, v=epd.version))
    
    for file_name in argv:
        if not os.path.exists(file_name):
            sys.exit('error: image file{f:s} does not exist'.format(f=file_name))
        print('display: {f:s}'.format(f=file_name))
        display_file(epd, file_name)


def display_file(epd, file_name):
    """display centre of image then resized image"""

    background = Image.new('1', epd.size,1)  #create white background
    image = Image.open(file_name)
    image = ImageOps.grayscale(image)
    imageSize = 120,120   #size of the thumbnail

    draw = ImageDraw.Draw(background)

    myFont = ImageFont.truetype(filename = FONT_FILE, size=FONT_SIZE)  #define the size and ont of text


    draw.text((0,120), 'NAME: '+VISITOR_NAME, fill=0, font=myFont)
    draw.text((0,145), 'OCCUPATION: '+VISITOR_OCCUPATION , fill=0, font=myFont)

    bw = image.convert("1", dither=Image.FLOYDSTEINBERG)
    image.thumbnail(imageSize, Image.ANTIALIAS)
    background.paste(image,(0,0)) #paste the thumbnail on top of white background

    epd.display(background)
    epd.update()


    time.sleep(3) # delay in seconds



# main
if "__main__" == __name__:
  if len(sys.argv) < 2:
        sys.exit('usage: {p:s} image-file'.format(p=sys.argv[0]))
    main(sys.argv[1:])



